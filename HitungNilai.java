import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.Math;

public class HitungNilai
{
	public static double Jumlah(double [] nilai)
	{
		double hasil;
		hasil=0;
		for (int i=0;i<nilai.length ; i++)
		{
			hasil=hasil + nilai[i];
		}
		return hasil;
	}
	
	public static double rata(double [] nilai)
	{
		double rata;
		rata = Jumlah(nilai)/(nilai.length); 
		return rata;
	}
	
	public static double sd (double [] nilai)
	{
		double rata = rata(nilai);
		double jm = 0;
		for (int k=0;k<nilai.length;k++)
		{
			jm += Math.pow(nilai[k] - rata,2);
		}
		return Math.sqrt(jm)/(nilai.length);
	}
	
	public static void main(String[] args) 
	{
		try 
		{
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("\nBanyak Nilai = ");
			int a = Integer.parseInt(input.readLine());
			double[] arrayNilai = new double[a];
			for(int i=0;i<a;i++)
			{
				System.out.print("Nilai " +(i+1)+":");
				arrayNilai[i] = Double.parseDouble(input.readLine());
			}
			System.out.println("Rata - rata :"+rata(arrayNilai));
			System.out.println("Standar Deviasi :"+sd(arrayNilai));
		} 
		catch (IOException e) 
		{
			System.out.println("Inputan salah");
		}
	}
}
