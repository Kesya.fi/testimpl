import java.io.BufferedReader;
import java.io.FileReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.File;

public class FileReading
{
	public static void main(String[] args)
	{
		String line = "", fileContent = "";
		try 
		{
			BufferedReader fileInput = new BufferedReader (new FileReader(new File("D:\\Satrio\\Kuliah\\Semester 4\\Pemrograman Objek 2\\Praktikum\\Modul VIII\\Test.txt")));
			line = fileInput.readLine();
			fileContent = line + "\n";
			while(line != null)
			{
				line = fileInput.readLine();
				if(line != null) fileContent += line + "\n";
			}
			fileInput.close();
		} 
		catch (EOFException eofe) 
		{
			System.out.println("No more lines to read.");
			System.exit(0);
		}
		catch (IOException ioe) 
		{
			System.out.println("Error reading file.");
			System.exit(0);
		}
		System.out.println(fileContent);
	}
}
